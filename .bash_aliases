####
#
# This is a function made by Atauã Pinali Doederlein to
# automate initialization of React Apps.
#
# Set your Gitlab user on line 15 to automatically create a remote
# repository -- using ssh! -- after project creation
#
# Just call 'cra', enter the project name and go grab some snacks!
#
####

function cra() {
   # set your Gitlab user here (the content after the @ from your profile)
   gitlabUser='ataua_doederlein'
   #
   echo -n "Insert new project name: "
   read projectName
   npx create-react-app ${projectName/' '/'-'}
   cd ${projectName/' '/'-'}
   clear
   echo "${projectName/' '/'-'} criado com sucesso. Configurando VSCode -- Linter & Preetier"
   npm install --save-dev eslint-config-universe
   npm install --save-dev typescript prettier @typescript-eslint/eslint-plugin @typescript-eslint/parser
   npm audit fix
   echo {'printWidth': 100, 'tabWidth': 2, 'singleQuote': true, 'jsxBracketSameLine': true, 'trailingComma': 'es5'} >.prettierrc
   clear
   echo 'configurações finalizadas. Iniciando repositório em Gitlab...'
   git add .
   git commit -m 'initial'
   git push --set-upstream git@gitlab.com:$gitlabUser/$(git rev-parse --show-toplevel | xargs basename).git $(git rev-parse --abbrev-ref HEAD)
   printf %b "Projeto ${projectName/' '/'-'} iniciado com sucesso, com repositório em https://gitlab.com/$gitlabUser/${projectName/' '/'-'}\n\nNo arquivo package.json, troque o conteúdo do \"eslintConfig\".\nDeve ficar assim (copie e cole):\n\n\"eslintConfig\": {\n\t\"extends\": \"universe/web\",\n\t\"rules\": {\n\t\t\"react/no-did-mount-set-state\": \"off\"\n\t}\n},\n"
}

####
#
# Some other stuff
#
####

# navigating
alias cd.='cd .'
alias cd..='cd ..'

# git stuff
alias a.='git add .'
alias c.='git commit -m'
alias u.='git push -u origin master'

# initiate HTML basic structure
alias all='touch index.html style.css script.js'
